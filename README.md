# Mitsubishi Heavy Industries Air Conditioner to ESP8266 PCB

This is a simple PCB, designed in KiCad, for connecting an ESP8266 to the CNS port of a Mitsubishi Heavy Industries Air Conditioner. Inspired by the work of [absalom-muc](https://github.com/absalom-muc) on their [MHI-AC-Ctrl](https://github.com/absalom-muc/MHI-AC-Ctrl) project.


## Compatible software

The PCB is designed to run some variant of the [MHI-AC-Ctrl](https://github.com/absalom-muc/MHI-AC-Ctrl) code – either directly, or via ESPHome through e.g. [MHI-AC-Ctrl-ESPHome](https://github.com/ginkage/MHI-AC-Ctrl-ESPHome) from [Ivan Podogov](https://github.com/ginkage). 

Since this board is intended to be permanently mounted within or near the air conditioner unit, I haven't included a physical "boot" or "reset" button – instead both are provided as bare pads. To "press" these buttons, just short the pads with a wire or some tweezers. To flash the firmware of your choice, use either the Arduino tools or esptool.py as per usual for an ESP8266.


## Further reading

This design was based heavily on the notes on the [hardware](https://github.com/absalom-muc/MHI-AC-Ctrl/blob/master/Hardware.md) page of the MHI-AC-Ctrl project. It's worth reading through that if you haven't seen it before, it contains lots of good information.


## Assembly

The BOM has been filled out in KiCad for the PCB to be manufactured and assembled by JLCPCB – except for the CUI [PXO7803-500-S](https://www.cui.com/product/resource/pxo78-500-s.pdf) power supply. Since this is a three pin through-hole part I figured it would be easier and cheaper to do that after receiving the PCBAs.


## Licence

This work is licensed under the terms of the CERN Open Hardware Licence Version 2 - Strongly Reciprocal.


## PCB Renders

![PCB Front](./renders/MHI-ESP8266_front.png)
![PCB Back](./renders/MHI-ESP8266_back.png)

